# dockerizing a flask app

Following instructions here:
http://containertutorials.com/docker-compose/flask-simple-app.html

https://docs.docker.com/get-started/part2/#build-the-app

in the 'web' directory:

```docker build -t friendlyhello .```

```docker run -p 4000:80 friendlyhello```

# allow for local updates
```docker run -p 4000:80 -v [hostdir]:/app friendlyhello```

# allow for GPIO access

http://forum.goatech.org/t/installing-and-using-docker-on-raspberry-pi/136

# allow for USB access

```docker run -t -i --device=/dev/ttyUSB0 ubuntu bash```

# run with USB access

```docker run -p 4000:80 -v [hostdir]:/app -t -i --device=/dev/ttyACM0 cindy1```


