from flask import Flask, render_template
from redis import Redis, RedisError
import os
import socket
import cindymod
import time
from subprocess import call

# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template("main.html")


@app.route('/background_process_test')
def background_process_test():
    try:
        #call(["omxplayer","/home/pi/gitwork/dockerized-flask-app/web/static/peepikiki.mp3"]) 
        port="/dev/ttyACM0"
        print("Connected to",port)
        peep=cindymod.Peep(port)
        orig=90
        
        endpoint = 135
 
        # reset position
        peep.flush()
        time.sleep(1)
        peep.servo_place(orig)
        time.sleep(2)

        peep.servo_place(endpoint) 
        time.sleep(2)
        for i in range(0,3): 
            peep.servo_place(endpoint-10)
            peep.flush()
            time.sleep(.1)
            peep.servo_place(endpoint+10)
            peep.flush()
            time.sleep(.5)

        peep.servo_place(orig)
    except RedisError:
        print("oops")

    return 'Hello, World!'
    
@app.route("/cindy")
def cindy():
    try:
        print("hello")
        port="/dev/ttyACM0"
        print("Connected to",port)
        peep=cindymod.Peep(port)
        orig=90
	endpoint = 135
        peep.flush()
        peep.flush()
        time.sleep(1)
        peep.servo_place(orig)
        time.sleep(2)
    
        peep.servo_place(endpoint)
   
        for i in range(0,3): 
            peep.servo_place(endpoint-10)
            peep.flush()
            time.sleep(.1)
            peep.servo_place(endpoint+10)
            peep.flush()
            time.sleep(.5)

        peep.servo_place(orig)

    except RedisError:
        print("oops")

    return 'Hello, World!'


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)

